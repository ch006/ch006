
#include<stdio.h>
int main()
{
    int beg,end, mid, pos, num, i, n, found=0;
    printf("enter the no. of elements in the array\n");
    scanf("%d",&n);
    int arr[n];
    printf("enter the elements of the array;\n");
    for(i=0;i<n;i++)
    {
        scanf("%d",&arr[i]);
    }
    printf("enter the number that has to be searched:\n");
    scanf("%d",&num);
    beg=0;
    end=n-1;
    while(beg <= end)
    {
        mid=(beg+end)/2;
        if(arr[mid]== num)
        {
            printf("%d is present in the array at position %d \n",num,mid);
            found=1;
            break;
        }
        if(arr[mid]>num)
        {
            end = mid-1;
        }
        else if(arr[mid]<num)
        {
            beg = mid+1;
        }
    }
    if(found==0)
        printf("%d does not exist in the array", num);
    return 0;
}
    