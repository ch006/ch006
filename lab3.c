
#include<stdio.h>
#include<math.h>
int main()
{
int a,b,c;
float d,r1,r2;
printf("Enter the coefficients of the quadratic equations:\n");
scanf("%d%d%d",&a,&b,&c);
d=sqrt(b*b-4*a*c);
r1=(-b+sqrt(b*b-4*a*c))/2*a;
r2=(-b-sqrt(b*b-4*a*c))/2*a;
if (d==0)
{
printf("Roots are equal");
printf("%f%f",r1,r2);
}
else if (d>0)
{
printf("Roots are real and distinct");
printf("%f%f",r1,r2);
}
else 
{
printf("Roots are imaginary");
}
return 0;
}



#include<stdio.h>
float convert(float);
float main()
{
float r, a;
printf("enter the radius of a circle r");
scanf("%f", &r);
a=convert(r);
printf("area of circle is %f", a);
return 0;
}
float convert(float r)
{
float area;
area=3.14*r*r;
return area;
}