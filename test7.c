
#include<stdio.h>
int main()
{
    struct student
    {
        char name[20];
        int r_no;
        float marks;
        char dob[30];
        float fee;
    };
    struct student stud1, stud2 ;
    printf("ENTER STUDENT 1 DETAILS\n");
    printf("ENTER THE NAME: \n");
    scanf("%s", stud1.name);
    printf("ENTER ROLL NUMBER: \n");
    scanf("%d", &stud1.r_no);
    printf("ENTER MARKS: \n");
    scanf("%f", &stud1.marks);
    printf("ENTER DATE OF BIRTH: \n");
    scanf("%s", stud1.dob);
    printf("ENTER FEES: \n");
    scanf("%f", &stud1.fee);
    printf("\n");
    printf("ENTER STUDENT 2 DETAILS \n");
    printf("ENTER THE NAME: \n");
    scanf("%s", stud2.name);
    printf("ENTER ROLL NUMBER: \n");
    scanf("%d", &stud2.r_no);
    printf("ENTER MARKS: \n");
    scanf("%f", &stud2.marks);
    printf("ENTER DATE OF BIRTH: \n");
    scanf("%s", stud2.dob);
    printf("ENTER FEES: \n");
    scanf("%f", &stud2.fee);
    printf("\n");
    printf("NAME: %s \nROLL NUMBER: %d \nMARKS: %f \nDATE OF BIRTH: %s \nFEE: %f\n", stud1.name, stud1.r_no, stud1.marks, stud1.dob, stud1.fee);
    printf("\n");
    printf("NAME: %s \nROLL NUMBER: %d \nMARKS: %f \nDATE OF BIRTH: %s \nFEE: %f\n", stud2.name, stud2.r_no, stud2.marks, stud2.dob, stud2.fee);
    if(stud1.marks > stud2.marks)
        printf("%s has scored the highest marks", stud1.name);
    else
        printf("%s HAS SCORED HIGHEST MARKS", stud2.name);
    return 0;
}       